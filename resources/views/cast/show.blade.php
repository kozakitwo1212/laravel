@extends('adminlte.master')
@section('content')

<div class="ml-3">
Show Cast {{$cast->id}}
<h4 class="text-primary">{{$cast->nama}}</h4>
<p>{{$cast->umur}} years old</p>
<p>{{$cast->bio}}</p>
</div>
@endsection